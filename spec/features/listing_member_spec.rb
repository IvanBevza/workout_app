require 'rails_helper'

RSpec.describe 'Listing members' do
  let!(:user) { create(:user) }
  let!(:second_user) { create(:user) }

  scenario 'shows a list of registered members' do
    visit '/'

    expect(page).to have_content('List of members')
    expect(page).to have_content(user.first_name)
    expect(page).to have_content(user.last_name)
    expect(page).to have_content(second_user.first_name)
    expect(page).to have_content(second_user.last_name)
  end
end