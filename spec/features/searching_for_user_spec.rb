require 'rails_helper'

RSpec.describe 'Searching for user' do
  let(:user) { create(:user) }
  let(:second_user) { create(:user) }

  scenario 'with existing name returns all those users' do
    visit '/'

    fill_in 'search_name', with: user.first_name
    click_button 'Search'

    expect(page).to have_content(user.first_name)
    expect(page).to have_content(user.last_name)

    expect(current_path).to eq("/dashboard/search")
  end
end