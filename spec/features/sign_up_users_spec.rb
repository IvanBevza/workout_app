require 'rails_helper'

RSpec.describe 'Sign up users' do
  let(:user) { build(:user) }

  scenario 'with valid credentials' do
    visit '/'

    click_link 'Sign up'

    fill_in "Email", with: user.email
    fill_in "First name", with: user.first_name
    fill_in "Last name", with: user.last_name
    fill_in "Password", with: user.password
    fill_in "Password confirmation", with: user.password

    click_button 'Sign up'

    expect(page).to have_content('You have signed up successfully')
  end

  scenario 'with invalid credentials' do
    visit '/'

    click_link 'Sign up'

    fill_in "First name", with: ""
    fill_in "Last name", with: ""
    fill_in "Email", with: "user@example.com"
    fill_in "Password", with: "password"
    fill_in "Password confirmation", with: "password"

    click_button 'Sign up'

    expect(page).to have_content("First name can't be blank")
    expect(page).to have_content("Last name can't be blank")
  end
end

