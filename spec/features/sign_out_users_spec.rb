require 'rails_helper'

RSpec.describe 'User sign out' do
  let(:user) { create(:user) }

  before do
    visit '/'

    click_link 'Sign in'

    fill_in "Email", with: user.email
    fill_in "Password", with: user.password

    click_button 'Log in'
  end

  scenario 'successfully' do
    visit '/'

    click_link 'Sign out'

    expect(page).to have_content('Signed out successfully.')
  end
end

