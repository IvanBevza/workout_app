require 'rails_helper'

RSpec.describe 'Following friends' do
  let!(:user) { create(:user) }
  let!(:second_user) { create(:user) }

  before do
    login_as(user)
  end

  scenario 'if sign in succeeds' do
    visit '/'

    expect(page).to have_content(user.full_name)
    expect(page).to have_content(second_user.last_name)
    expect(page).not_to have_link("Follow, href: '/friendships?friend_id=#{user.id}'")

    link = "a[href='/friendships?friend_id=#{second_user.id}']"
    find(link).click

    expect(page).not_to have_link("Follow, href: '/friendships?friend_id=#{second_user.id}'")
  end
end