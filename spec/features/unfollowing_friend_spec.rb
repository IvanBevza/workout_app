require "rails_helper"

RSpec.feature "Unfollowing friend" do
  let(:user) { create(:user) }
  let(:second_user) { create(:user) }

  before do
    login_as(user)
    @following = Friendship.create(user: user, friend: second_user)
  end

  scenario do
    visit '/'

    click_link 'My lounge'

    link = "a[href='/friendships/#{@following.id}'][data-method='delete']"
    find(link)

    expect(page).to have_content(second_user.full_name)
  end
end