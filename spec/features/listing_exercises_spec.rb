require 'rails_helper'

RSpec.describe 'Listing exercises' do
  let(:user) { create(:user) }
  let(:second_user) { create(:user) }

  before do
    login_as(user)

    @ex1 = user.exercises.create(duration_in_min: 20, workout: 'Running', workout_date: Date.today)
    @ex2 = user.exercises.create(duration_in_min: 20, workout: 'Swimming', workout_date: Date.today)

    @following = Friendship.create(user: user, friend: second_user)
  end

  scenario "shows user's workout for last 7 days" do
    visit '/'

    click_link 'My lounge'

    expect(page).to have_content(@ex1.duration_in_min)
    expect(page).to have_content(@ex1.workout)
    expect(page).to have_content(@ex1.workout_date)

    expect(page).to have_content(@ex2.duration_in_min)
    expect(page).to have_content(@ex2.workout)
    expect(page).to have_content(@ex2.workout_date)
  end

  scenario 'shows a list of users friends' do
    visit '/'

    click_link 'My lounge'

    expect(page).to have_content('My friends')
    expect(page).to have_link(second_user.full_name)
    expect(page).to have_link('Unfollow')
  end
end