require 'rails_helper'

RSpec.describe 'Sign in users' do
  let(:user) { create(:user) }

  scenario 'with valid credentials' do
    visit '/'

    click_link 'Sign in'

    fill_in "Email", with: user.email
    fill_in "Password", with: user.password

    click_button 'Log in'

    expect(page).to have_content('Signed in successfully.')
  end
end

