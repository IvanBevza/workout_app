require 'rails_helper'

RSpec.describe 'Editing exercise' do
  let(:user) { create(:user) }

  before do
    @user_exercise = user.exercises.create!(duration_in_min: 45, workout: 'Cardio', workout_date: Date.today)
    login_as(user)
  end

  scenario 'with valid inputs' do
    visit '/'

    click_link 'My lounge'

    link = "a[href='/users/#{user.id}/exercises/#{@user_exercise.id}/edit']"

    find(link).click

    fill_in 'Duration', with: 70

    click_button 'Create exercise'

    expect(page).to have_content('Exercise has been updated')
    expect(page).to have_content('70')
    expect(page).not_to have_content('50')
  end
end