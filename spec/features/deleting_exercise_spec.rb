require 'rails_helper'

RSpec.describe 'Deleting exercise' do
  let(:user) { create(:user) }
  let(:exercise) { create(:exercise) }

  before do
    @user_exercise = user.exercises.create!(duration_in_min: 45, workout: 'Cardio', workout_date: Date.today)
    login_as(user)
  end

  scenario 'with valid inputs' do
    visit '/'

    click_link 'My lounge'

    link = "//a[contains(@href, '/users/#{user.id}/exercises/#{@user_exercise.id}') and .//text()='Destroy']"
    find(:xpath, link).click

    expect(page).to have_content('Exercise has been deleted')

  end
end