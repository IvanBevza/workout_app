require "rails_helper"

RSpec.feature "Showing Friend Workout" do
  let(:user) { create(:user) }
  let(:second_user) { create(:user) }

  before do
    @e1 = user.exercises.create(duration_in_min: 74, workout: "Weight lifting routine", workout_date: Date.today)

    @e2 = second_user.exercises.create(duration_in_min: 55, workout: "Sarahs weight lifting routine", workout_date: Date.today)

    login_as(user)

    @following = Friendship.create(user: user, friend: second_user)
  end

  scenario "shows friend's workout for last 7 days" do
    visit "/"

    click_link "My lounge"
    click_link second_user.full_name

    expect(page).to have_content(second_user.full_name + "'s exercises")
    expect(page).to have_content(@e2.workout)
    expect(page).to have_css("div#chart")
  end
end