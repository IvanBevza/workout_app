Rails.application.routes.draw do
  devise_for :users, controller: { registrations: 'registrations' }

  root 'dashboard#index'

  resources :dashboard

  resources :users do
    resources :exercises
  end

  resource :dashboard, only: [:index] do
    collection do
      post :search, to: 'dashboard#search'
    end
  end

  resources :friendships, only: [:show, :create, :destroy]
end
