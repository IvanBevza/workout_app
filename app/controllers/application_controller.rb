class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    user_attr = [:first_name, :last_name, :email, :password, :password_confirmation]

    devise_parameter_sanitizer.permit :account_update, keys: user_attr
    devise_parameter_sanitizer.permit :sign_up, keys: user_attr
  end
end
